---
title: Hello, World!
date: 2025-02-14
description: Hey! My name is Jorys, and welcome to my website!
---

Hello, World! Welcome to the first post on this new website. I'm Jorys, aka ChaineJoueur542 everywhere (yes, it's a randomly generated username).

I'm a web developer and IT technician in the northeast of France, and I do lots of things on the Internet.

On this website, you'll find everything I do on the Internet in my personal time (I leave the professional to my professional site): my projects, my blog posts, my videos (soon I hope!) and everything else.

Thanks for visiting and see you soon, maybe!
