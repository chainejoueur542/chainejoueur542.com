---
title: Bonjour tout le monde !
slug: bonjour-tout-le-monde
date: 2025-02-14
description: Salut ! Moi c'est Jorys, bienvenue sur mon site !
---

Bonjour tout me monde ! Bienvenue sur le premier article de ce nouveau site web. Moi c’est Jorys, alias ChaineJoueur542 un peu partout (oui, c’est un pseudo généré aléatoirement).

Je suis un développeur web et technicien informatique dans le nord-est de la France, et je fais plein de trucs sur Internet.

Sur ce site, vous y retrouverez tout ce que je fais sur Internet dans le cadre personnel (je laisse le professionnel pour mon site pro) : mes projets, mes articles, mes vidéos (bientôt j’espère !) et tout le reste.

Merci de votre visite et à bientôt peut-être !
